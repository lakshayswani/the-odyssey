#ifndef FC_PLANETPS
#define FC_PLANETPS

inline PlanetData GetPlanetData(PlanetOutput p)
{
	PlanetData pData;

	pData.NormalDir = normalize(p.normalDir);	
	pData.LightDir = normalize(_WorldSpaceLightPos0);
	pData.LightDirTS = normalize(WorldToTangent(pData.LightDir, p));
	pData.ViewDir = normalize(_WorldSpaceCameraPos.xyz - p.posWorld.xyz);

	pData.UV = p.uv;
	pData.ScatterUV = ScatterUV(pData.NormalDir, pData.LightDir, pData.ViewDir);
	float4 uvScaled = float4(pData.UV.x * 2, pData.UV.y, pData.UV.z * 0.5, pData.UV.w * 0.5);

	pData.GradientMap = tex2D(_GradientMap, pData.UV.xy).r;
	
	pData.NormalMap = (UnpackPlanetNormal((MixPoleMap(_SurfaceNormalMap, pData.GradientMap, uvScaled, _HeightTile)).xyz, _NormalHeight));
	pData.NormalMap = normalize(TangentToWorld(pData.NormalMap, p));
	
	pData.OceanMap = tex2D(_OceanMask, pData.UV.xy).rg;
	pData.HeightMap = MixPoleMap(_HeightMap, pData.GradientMap, uvScaled, _HeightTile);
	 
	pData.SunsetMap = tex2D(_SunsetMap, pData.ScatterUV);	

	pData.DiffuseFactor = saturate(dot(pData.LightDir, pData.NormalDir));
	pData.DiffuseFactorNM = saturate(dot(pData.LightDir, pData.NormalMap));

	pData.Ambient = UNITY_LIGHTMODEL_AMBIENT.rgb;
	
	pData.shadow =1;
	
	pData.DiffuseFactor *= pData.shadow;
	pData.DiffuseFactorNM *= pData.shadow;

	return pData;
}



inline float3 AddBaseColor(PlanetData p)
{
	return p.HeightMap.r * _TintColor.rgb;
}

inline float3 AddVegetationColor(float3 color, PlanetData p)
{
	float3 vegColor = p.HeightMap.g * _VegetationColor.rgb;
	float mask = (_VegetationCoverage - p.HeightMap.b * p.HeightMap.r) * _VegetationFactors;
	mask = saturate(lerp(0, 1, mask));
	return lerp(color, vegColor, mask);
}

inline float3 AddDesertColor(float3 color, PlanetData p)
{
	float3 desColor = p.HeightMap.b  * _DesertColor.rgb;
	float mask = (_DesertCoverage - p.HeightMap.b * p.HeightMap.r) * _DesertFactors;
	mask = saturate(lerp(0, 1, mask));
	return lerp(color, desColor, mask);
}

inline float3 AddMountainColor(float3 color, PlanetData p)
{
	float3 mntColor = p.HeightMap.r * _MountainColor.rgb;
	float mask = (_MountainCoverage - p.HeightMap.b) * _MountainFactors;
	mask = saturate(lerp(0, 1, mask));
	return lerp(color, mntColor, mask);
}

inline float3 AddWater(float3 color, PlanetData p)
{	
	float depth = saturate(pow(p.HeightMap.b * p.HeightMap.r, _WaterDetailPow) * _WaterDetailMult);
	float shoreMask = saturate(pow(p.OceanMap.g, _ShoreFactor));

	float3 waterColor = lerp(_DeepWaterColor, _ShallowWaterColor, depth);
	waterColor = saturate(lerp(_ShoreColor.rgb + waterColor, waterColor, shoreMask));

	float waterSpec = saturate(dot(p.LightDir, -reflect(p.ViewDir, p.NormalDir)));
	waterSpec = saturate(pow(waterSpec, _WaterSpecularPow) * _WaterSpecularMult);

	float waterFresnel = saturate(pow(saturate(1 - dot(p.ViewDir, p.NormalDir)), _WaterFresnelPow) * _WaterFresnelMult);

	waterColor += (waterSpec + waterFresnel) * _WaterSpecularColor;
	return lerp(color, saturate(waterColor), p.OceanMap.r);	
}

inline float3 AddDiffuse(float3 color, PlanetData p)
{
	float diffuse = lerp(p.DiffuseFactorNM, p.DiffuseFactor,  p.OceanMap.r * 1);
	return color * (pow(diffuse, _DiffusePow) + 0.0);
}

inline float3 AddFresnelLand(float3 color, PlanetData p)
{
	float fresnel = saturate(pow(saturate(1 - dot(p.ViewDir, p.NormalMap)), _FresnelLandPow) * _FresnelLandMult);
	return color + (fresnel * (1 - p.OceanMap.r) * p.DiffuseFactor) * _LandSpecularColor.rgb;
}

inline float3 AddCloudsFlat(float3 color, PlanetData p)
{
	float2 shadowUVBelly = -p.LightDirTS.xy * 0.005;
	float2 shadowUVPole = -p.LightDir.xy * 0.005;

	float2 bellyUV = p.UV.xy * 1;
	bellyUV.x += _CloudsAnimation * 0.15 * _Time;
	float2 poleUV = RotateUV(p.UV.zw, _Time, -_CloudsAnimation);

	float cloudBelly = tex2D(_CloudsMap, bellyUV * float2(6, 3)).r;
	float cloudPole = tex2D(_CloudsCapMap, poleUV).r;
	float cloudMix = lerp(cloudPole, cloudBelly, p.GradientMap + 0.1);

	float cloudBellyShadow = tex2D(_CloudsMap, (bellyUV + shadowUVBelly) * float2(6, 3)).r;
	float cloudPoleShadow = tex2D(_CloudsCapMap, poleUV + shadowUVPole).r;
	float cloudMixShadow = lerp(cloudPoleShadow, cloudBellyShadow, p.GradientMap + 0.1);
	
	cloudMixShadow = saturate(pow(1 - cloudMixShadow, _CloudsShadows));

	color *= cloudMixShadow;
	color += max(p.shadow, 0.005) * saturate(cloudMix * (0.002 + pow(p.SunsetMap, _CloudsSunset)) * _CloudsBrightness * _CloudsColor.rgb);
	return color;
}

inline float3 AddClouds(float3 color, PlanetData p)
{
	float2 shadowUVBelly = -p.LightDirTS.xy * 0.005;
	float2 shadowUVPole = -p.LightDir.xy * 0.005;

	float2 bellyUV = p.UV.xy * 1;
	bellyUV.x += _CloudsAnimation * 0.15 * _Time;
	float2 poleUV = RotateUV(p.UV.zw, _Time, -_CloudsAnimation);

	float cloudBelly = tex2D(_CloudsMap, bellyUV * float2(2, 1)).r;
	float cloudPole = tex2D(_CloudsCapMap, poleUV).r;
	float cloudMix = lerp(cloudPole, cloudBelly, p.GradientMap + 0.1);

	float cloudBellyShadow = tex2D(_CloudsMap, (bellyUV + shadowUVBelly) * float2(2, 1)).r;
	float cloudPoleShadow = tex2D(_CloudsCapMap, poleUV + shadowUVPole).r;
	float cloudMixShadow = lerp(cloudPoleShadow, cloudBellyShadow, p.GradientMap + 0.1);

	cloudMixShadow = saturate(pow(1 - cloudMixShadow, _CloudsShadows));

	color *= cloudMixShadow;
	color += max(p.shadow, 0.005) * saturate(cloudMix * (0.002 + pow(p.SunsetMap, _CloudsSunset)) * _CloudsBrightness * _CloudsColor.rgb);
	return color;
}

inline float3 AddCityLights(float3 color, PlanetData p)
{
	float dayNightFactor = max(p.LightDirTS.z, 0);
	float mask = tex2D(_CityLightMaskMap, p.UV.xy).x;
	float2 detailUV = tex2D( _CityLightUVMap, p.UV.xy ).xy;
	float cityLightMap = tex2D(_CityLightMap, detailUV).x * mask;
	float3 cityLight = pow(cityLightMap, 1 / _Population) * _CityLightColor.rgb * (1 - p.OceanMap.x) * (1 - dayNightFactor);

	return color + cityLight * 2;
}



#endif