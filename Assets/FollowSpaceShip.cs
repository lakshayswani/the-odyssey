﻿using UnityEngine;

public class FollowSpaceShip : MonoBehaviour
{
    // Start is called before the first frame update
    GameObject CameraPosition;
    Vector3 temp;
    public bool withChair = false;
    private ChairInputWrapper cInput;
    void Start()
    {
        GameObject vm = GameObject.Find("Voyager Manager");
        if(vm!=null)
            cInput = vm.GetComponent<ChairInputWrapper>();
    }

    // Update is called once per frame
    void Update()
    {
        if(withChair)
        {
            if(cInput!=null)
                transform.Rotate(cInput.Delta_pitch, -cInput.Delta_yaw, 0);
        }
        else
        {
            //Camera.main.transform.parent = GameObject.Find("spaceShip").transform;
            this.enabled = false;
        }
        
    }
}
