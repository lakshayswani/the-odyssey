﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
public class QuadController : MonoBehaviour
{
    private GameObject quad;
    private GameObject health;
    private GameObject cylinder;
    private CylinderHealthBar chb; //= GameObject.Find("spaceShip").GetComponent<CylinderHealthBar>();
    private GameObject forcefield; //= GameObject.Find("ForceField");
    private GameObject crashlight;
    private Animator quadAnim;
    public bool enableAsteroids = false;
    public bool enableControl = false;
    private AudioSource[] soundSource=null;
    ScoreBoard sb;

    public bool displayedScore = false;

    // Start is called before the first frame update

    public void Start()
    {
        foreach (Transform child in GameObject.Find("cockpit").GetComponentInChildren<Transform>())
        {
            if (child.gameObject.name == "Quad")
            {
                quad = child.gameObject;
                break;
            }
        }
        chb = GameObject.Find("spaceShip").GetComponent<CylinderHealthBar>();
        forcefield = GameObject.Find("ForceField");
        crashlight = GameObject.Find("CrashLight");
        if(quad!=null)
            quadAnim = quad.GetComponent<Animator>();
        //StartCoroutine(BroadCastInitialMessage());
        GameObject ss = GameObject.Find("SoundSource");
        if(ss!=null)
            soundSource = ss.GetComponents<AudioSource>();
        sb = GameObject.Find("spaceShip").GetComponent<ScoreBoard>();
    }

    public IEnumerator DisplayRideChoice()
    {
        quad.SetActive(true);
        quadAnim.SetTrigger("buttonincoming");
        yield return new WaitForSeconds(1.0f);

    }
    public IEnumerator BroadCastInitialMessage()
    {
        // Initial Message
        quad.SetActive(true);
        quadAnim.SetTrigger("incoming");
        yield return new WaitForSeconds(1.0f);
        soundSource[1].Play();
        quadAnim.SetTrigger("active");
        soundSource[4].Play();
        yield return new WaitForSeconds(15f);
        soundSource[1].Play();
        quadAnim.SetTrigger("closing");
        yield return new WaitForSeconds(1.0f);
        quad.SetActive(false);

        yield return new WaitForSeconds(1.0f);
    }

    public IEnumerator AutoPilotMessage()
    {
        // Auto Pilot Message
        quad.SetActive(true);
        quadAnim.SetTrigger("incoming");
        yield return new WaitForSeconds(1.0f);
        soundSource[1].Play();
        quadAnim.SetTrigger("autpilot");
        yield return new WaitForSeconds(0.3f);
        soundSource[5].Play();
        yield return new WaitForSeconds(2.7f);
        soundSource[1].Play();
        quadAnim.SetTrigger("disable");
        yield return new WaitForSeconds(1.0f);
        quad.SetActive(false);
        enableControl = true;
    }


    public IEnumerator AlertLight()
    {
        for (int i = 0; i < 4; i++)
        {
            crashlight.GetComponent<Light>().enabled = !crashlight.GetComponent<Light>().enabled;
            yield return new WaitForSeconds(1.0f);
        }
    }

    public IEnumerator AsteroidAlert()
    {
        // Asteroid Alert
        quad.SetActive(true);
        
        quadAnim.SetTrigger("incoming");
        yield return new WaitForSeconds(1.0f);
        soundSource[1].Play();
        quadAnim.SetTrigger("asteroid");
        StartCoroutine(AlertLight());
        soundSource[2].Play();
        yield return new WaitForSeconds(2.8f);
        soundSource[2].Stop();
        yield return new WaitForSeconds(0.2f);
        quadAnim.SetTrigger("closewarning");
        soundSource[1].Play();
        yield return new WaitForSeconds(1.0f);
        quad.SetActive(false);


        // Activating ForceField
        quad.SetActive(true);
        quadAnim.SetTrigger("incoming");
        yield return new WaitForSeconds(1.0f);
        soundSource[1].Play();
        quadAnim.SetTrigger("activating");
        soundSource[6].Play();
        yield return new WaitForSeconds(10f);
        //soundSource[3].Stop();
        //yield return new WaitForSeconds(0.3f);
        soundSource[1].Play();
        forcefield.GetComponent<MeshRenderer>().enabled = true;
        quadAnim.SetTrigger("closemess");
        yield return new WaitForSeconds(1.0f);
        quad.SetActive(false);

        // display health bar
        foreach (Transform child in GameObject.Find("cockpit").GetComponentInChildren<Transform>())
        {
            if (child.gameObject.name == "HealthBar")
            {
                health = child.gameObject;
                break;
            }
        }
        health.SetActive(true);
        GameObject.Find("spaceShip").GetComponent<CylinderHealthBar>().enabled = true;
        float y_inc = 0.0000283f;
        while (health.transform.localScale.y < 0.085)
        {
            health.transform.localScale += new Vector3(0, y_inc, 0);
        }
        foreach (Transform child in GameObject.Find("cockpit").GetComponentInChildren<Transform>())
        {
            if (child.gameObject.name == "Cylinder")
            {
                cylinder = child.gameObject;
                break;
            }
        }
        cylinder.SetActive(true);
        GameObject.Find("spaceShip").GetComponent<CylinderHealthBar>().enabled = true;
        while (cylinder.transform.localScale.y < 0.085)
        {
            cylinder.transform.localScale += new Vector3(0, y_inc, 0);
        }

        // stop displaying forcefield
        yield return new WaitForSeconds(1.0f);
        forcefield.GetComponent<MeshRenderer>().enabled = false;
        enableAsteroids = true;
        sb.startGame();
        if (GameObject.Find("UniversalSoundObject"))
        {
            AudioSource[] universalSoundObject = GameObject.Find("UniversalSoundObject").GetComponents<AudioSource>();
            universalSoundObject[1].Play();
        }
    }

    public IEnumerator DisplayScore(string score)
    {
        quad.SetActive(true);
        GameObject display = null;
        foreach (Transform child in quad.GetComponentInChildren<Transform>())
        {
            if (child.gameObject.name == "InitialMessage")
            {
                display = child.gameObject;
                break;
            }
        }
        if (display != null)
        {
            display.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "You reached the source of the signal!";
            quadAnim.SetTrigger("incoming");
            yield return new WaitForSeconds(1.0f);
            soundSource[1].Play();
            quadAnim.SetTrigger("active");
            yield return new WaitForSeconds(0.3f);
            soundSource[7].Play();
            yield return new WaitForSeconds(3.5f);
            soundSource[1].Play();
            quadAnim.SetTrigger("closing");
            yield return new WaitForSeconds(1.0f);
            quad.SetActive(false);

            quad.SetActive(true);
            display.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "You took..." + score;
            quadAnim.SetTrigger("incoming");
            yield return new WaitForSeconds(1.0f);
            soundSource[1].Play();
            quadAnim.SetTrigger("active");
            StartCoroutine(display.transform.GetChild(0).GetComponent<TeleType>().Type());
            yield return new WaitForSeconds(0.3f);
            //soundSource[5].Play();
            yield return new WaitForSeconds(1.7f);
            soundSource[1].Play();
            quadAnim.SetTrigger("closing");
            yield return new WaitForSeconds(1.0f);
            quad.SetActive(false);

            quad.SetActive(true);
            display.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Entering the atmosphere...";
            quadAnim.SetTrigger("incoming");
            yield return new WaitForSeconds(1.0f);
            soundSource[1].Play();
            quadAnim.SetTrigger("active");
            StartCoroutine(display.transform.GetChild(0).GetComponent<TeleType>().Type());
            yield return new WaitForSeconds(0.3f);
            soundSource[8].Play();
            yield return new WaitForSeconds(2.7f);
            soundSource[1].Play();
            quadAnim.SetTrigger("closing");
            yield return new WaitForSeconds(1.0f);
            quad.SetActive(false);

        }
        yield return new WaitForSeconds(2f);
        displayedScore = true;
    }
}
