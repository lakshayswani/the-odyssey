﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DistanceToLanding : MonoBehaviour
{
    private float yRadius = 375f;
    private float xRadius = 575f;
    private GameObject landingPad;

    // Start is called before the first frame update
    void Start()
    {
        landingPad = GameObject.Find("landing_pad");
    }

    // Update is called once per frame
    void Update()
    {
        GetDistance();
    }

    private void GetDistance()
    {
        float xDist = Mathf.Abs(landingPad.transform.position.x - gameObject.transform.position.x);
        float zDist = Mathf.Abs(landingPad.transform.position.z - gameObject.transform.position.z);
        Vector3 dir = landingPad.transform.position - gameObject.transform.position;
        float angle = Mathf.Atan2(zDist, xDist) * Mathf.Rad2Deg;
        Debug.Log(xDist+":"+zDist+":"+angle);
    }
}
