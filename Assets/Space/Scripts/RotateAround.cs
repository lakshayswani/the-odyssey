﻿using UnityEngine;
using System.Collections;

public class RotateAround : MonoBehaviour {
	public float rotSpeed;
	public bool thiIsPlanet; 
	public Transform Sun;
	public float rotAroundSunSpd;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		transform.Rotate(Vector3.up *rotSpeed* Time.deltaTime, Space.World);
		if(thiIsPlanet){
            Vector3 dir = transform.position - Sun.transform.position;
            dir.Normalize();
            Vector3 side = Vector3.Cross(dir, -transform.right);
            Vector3 cross = Vector3.Cross(dir, side);
            cross.Normalize();
            transform.RotateAround(Sun.transform.position, cross, rotAroundSunSpd * Time.deltaTime);
		}
	}
}
