using UnityEngine;
using System.Collections;

public class SpaceShipEngine : MonoBehaviour {
	public float enginePitch;
	private float smooth = 0.02f;
	public float maxPitch;
	public float minPitch;
	public bool EngineStarted;
	private AudioSource engineAS;
    private bool usingJoystick;

    GameObject velocityCylinder;
    private float maxScale;

	// Use this for initialization
	void Start () {
		engineAS = this.GetComponent<AudioSource>();
		enginePitch = 1;
        velocityCylinder = GameObject.Find("Velocity");
        maxScale = velocityCylinder.transform.localScale.y;
	}
	// Update is called once per frame
	void Update () {
		if(EngineStarted){
            float throttle = 0;
            if(usingJoystick)
            {
               throttle = Input.GetAxis("RightJoyStickVertical") * -1;
            }
            else
            {
                throttle = Input.GetAxis("Throttle");
            }
            if (throttle > 0){
			    enginePitch = Mathf.Lerp(enginePitch, maxPitch, smooth);
		    }
		    if(throttle < 0){
			    enginePitch = Mathf.Lerp(enginePitch, minPitch, smooth);
		    }
		    engineAS.pitch = enginePitch;
            changeVelocityScale(enginePitch);

        }
		else{
			engineAS.pitch = 0;
		}
	}

    public void setUsingJoyStick(bool joystick)
    {
        usingJoystick = joystick;
    }

    void changeVelocityScale(float pitch)
    {
        Vector3 temp = velocityCylinder.transform.localScale;
        temp.y = ((pitch - 0.999f) / 1.5f) * maxScale;
        velocityCylinder.transform.localScale = temp;
    }
}
