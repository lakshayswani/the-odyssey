﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidBelt : MonoBehaviour {
    GameObject spaceShip;
    List<GameObject> asteroid_pool = new List<GameObject>();
    [SerializeField]
    GameObject[] asteroids;

    [Header("Spawner Settings")]
    public int asteroidDensity;
    public float innerRadius;
    public float outerRadius;
    public float height;
    public bool rotatingClockwise;

    [Header("Asteroid Settings")]
    public float minOrbitSpeed;
    public float maxOrbitSpeed;
    public float minRotationSpeed;
    public float maxRotationSpeed;

    private Vector3 localPosition;
    private Vector3 worldOffset;
    private Vector3 worldPosition;
    private float randomRadius;
    private float randomRadian;
    private float x;
    private float y;
    private float z;


	// Use this for initialization
	void Start () {
        GenerateBelt();
    }

    void Update(){
        UpdateActiveAsteroids();
    }

    void GenerateBelt() {
        for (int i = 0; i < asteroidDensity; i++)
        {
            do
            {
                randomRadius = Random.Range(innerRadius, outerRadius);
                randomRadian = Random.Range(0, (2 * Mathf.PI));

                y = Random.Range(-(height / 2), (height / 2));
                x = randomRadius * Mathf.Cos(randomRadian);
                z = randomRadius * Mathf.Sin(randomRadian);
            }
            while (float.IsNaN(z) && float.IsNaN(x));

            localPosition = new Vector3(x, y, z);
            worldOffset = transform.rotation * localPosition;
            worldPosition = transform.position + worldOffset;

            GameObject obj = Instantiate(asteroids[Random.Range(0, asteroids.Length)], worldPosition, Quaternion.Euler(Random.Range(0, 360), Random.Range(0, 360), Random.Range(0, 360)));
            obj.AddComponent<BeltObject>().SetupBeltObject(Random.Range(minOrbitSpeed, maxOrbitSpeed), Random.Range(minRotationSpeed, maxRotationSpeed), gameObject, rotatingClockwise);
            obj.transform.SetParent(transform);
            asteroid_pool.Add(obj);
        }
    }


    void UpdateActiveAsteroids()
    {

        Renderer[] renderers;
        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(Camera.main);
        foreach (GameObject asteroid in asteroid_pool)
        {
            renderers = asteroid.GetComponentsInChildren<Renderer>();
            if (GeometryUtility.TestPlanesAABB(planes, renderers[0].bounds))
                asteroid.SetActive(true);
            else
                asteroid.SetActive(false);
        }
    }
    
}
