﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteeringJoystick : MonoBehaviour
{
    // Start is called before the first frame update
    GameObject leftSt;
    GameObject rightSt;
    private float maxAngle = 15f;
    private float leftXAngle;
    private float leftYAngle;
    private float rightXAngle;
    private float rightYAngle;

    void Start()
    {
        leftSt = GameObject.Find("leftSteering");
        rightSt = GameObject.Find("rightSteering");
    }

    // Update is called once per frame
    void Update()
    {
        ChangeJoysticks();
    }

    public void ChangeJoysticks() //float leftX, float leftY, float rightX, float rightY)
    {
        //strafe,pitch,yaw,target
        
        leftXAngle = Input.GetAxis("LeftJoyStickHorizontal") * maxAngle*-1;
        leftYAngle = Input.GetAxis("LeftJoyStickVertical") * maxAngle*-1;
        rightXAngle = Input.GetAxis("RightJoyStickHorizontal") * maxAngle*-1;
        rightYAngle = Input.GetAxis("RightJoyStickVertical") * maxAngle*-1;

        leftSt.transform.localEulerAngles = new Vector3(leftYAngle, 0, leftXAngle);
        rightSt.transform.localEulerAngles = new Vector3(rightYAngle, 0, rightXAngle);
    }
}
