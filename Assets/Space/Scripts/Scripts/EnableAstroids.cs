﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableAstroids : MonoBehaviour
{
    GameObject spaceShip;
    GameObject astroidBelt;
    GameObject astroidSphere;
    public float asteroidStartTime;
    public float DistressStartTime;
    private float time;

    public bool enableAstroid;
    public bool enableDistress;

    private bool astroidEnabled = false;
    private bool distressEnabled = false;
    // Start is called before the first frame update
    void Start()
    {
        time = 0f;
        spaceShip = GameObject.Find("spaceShip");
        astroidBelt = GameObject.Find("AsteroidBelt");
        astroidSphere = GameObject.Find("AsteroidSphere");
    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;
        if(enableAstroid && !astroidEnabled && time> asteroidStartTime)
        {
            //astroidBelt.transform.GetComponent<AsteroidBelt>().enabled = true;
            astroidSphere.transform.GetComponent<AsteroidField>().enabled = true;
            //astroidSphere.transform.position = spaceShip.transform.position + (spaceShip.transform.forward * 5000);

            astroidEnabled = true;
        }

        /*if (enableDistress && !distressEnabled && time > DistressStartTime)
        {
            GameObject.Find("Sphere").transform.GetChild(0).gameObject.SetActive(true);
            GameObject.Find("Sphere").GetComponent<signal>().enabled = true;
            distressEnabled = true;
        }*/

        if((astroidEnabled == enableAstroid) && (distressEnabled == enableDistress))
        {
            this.enabled = false;
        }
    }
}
