﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial : MonoBehaviour
{
    // Start is called before the first frame update
    private ShipInput input;
    private ShipPhysics physics;
    GameObject model;
    GameObject dummyModel;
    Rigidbody rb;
    public bool canControl;

    void Start()
    {
        input = GameObject.Find("spaceShip").GetComponent<ShipInput>();
        model = GameObject.Find("spaceShip_Model");
        dummyModel = (GameObject)Instantiate(model,model.transform.position,Quaternion.identity);
        dummyModel.transform.parent = model.transform.parent;
        dummyModel.transform.rotation = GameObject.Find("spaceShip").transform.rotation;
        dummyModel.GetComponent<SpaceShipModel>().enabled = false;
        model.SetActive(false);
        DisableParticleSystem();
        AddRigidBodyToModel();
        physics = dummyModel.AddComponent<ShipPhysics>();
        physics.LinearForceMultiplier = 0.01f;
        physics.angularForceMultiplier = 0.5f;
        canControl = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (canControl)
        {
            physics.SetPhysicsInput(new Vector3(input.strafe, input.verticalSpeed, input.throttle), new Vector3(input.pitch, input.yaw, input.roll));
        }
    }

    void AddRigidBodyToModel()
    {
        rb = dummyModel.AddComponent<Rigidbody>();
        rb.isKinematic = false;
        rb.useGravity = false;
    }

    void DisableParticleSystem()
    {
        foreach (Transform child in dummyModel.GetComponentInChildren<Transform>())
        {
            if (child.gameObject.name == "Particle System")
            {
                Disable(child.gameObject);
                break;
            }
        }
    }

    void Disable(GameObject go)
    {
        Destroy(go);
    }
}
