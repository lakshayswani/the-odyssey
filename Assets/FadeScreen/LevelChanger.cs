using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelChanger : MonoBehaviour {

	public Animator animator;

	private string levelToLoad;
	
	// Update is called once per frame
	void Update () {

	}



	public void FadeToLevel (string levelName)
	{
		levelToLoad = levelName;
		animator.SetTrigger("FadeOut");
        if (GameObject.Find("UniversalSoundObject"))
        {
            AudioSource[] universalSoundObject = GameObject.Find("UniversalSoundObject").GetComponents<AudioSource>();
            universalSoundObject[1].Pause();
        }
    }

	public void OnFadeComplete ()
	{
        if(levelToLoad == "Scene_1")
        {
            GameObject vm = GameObject.Find("Voyager Manager");
            if (vm != null)
            {
                ChairInputWrapper cw = vm.GetComponent<ChairInputWrapper>();
                PlayerPrefs.SetFloat("Chair_Yaw", cw.Yaw_chair_angle);
                PlayerPrefs.SetFloat("Chair_Pitch", cw.Pitch_chair_angle);
                Destroy(vm);
            }
            else
            {
                PlayerPrefs.SetFloat("Chair_Yaw", 0);
                PlayerPrefs.SetFloat("Chair_Pitch", 5);
            }
           
            GameObject api = GameObject.Find("_VoyagerDeviceAPI");
            if (api != null)
                Destroy(api);
        }
        SceneManager.LoadScene(levelToLoad);
        
	}
}
