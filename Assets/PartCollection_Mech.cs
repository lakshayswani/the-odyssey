﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PartCollection_Mech : MonoBehaviour
{


    public GameObject com1;
    public GameObject com2;
    public GameObject com3;

    List<GameObject> holo_parts = new List<GameObject>();
    public GameObject target;
    int partCount = 0;

    bool allPartCollectedDone = false;

    public float minDistToTarget = 50f;
    public float minAngleTowardsTarget = 10f;
    AudioSource[] soundSource;
    bool gameDone = false;

    MechControll mech;


    // Start is called before the first frame update
    void Start()
    {
        holo_parts.AddRange(GameObject.FindGameObjectsWithTag("mech_holoparts"));
        foreach(GameObject part in holo_parts)
        {
            part.SetActive(false);
        }
        GameObject ss = GameObject.Find("Scene4SoundObject");
        if (ss != null)
            soundSource = ss.GetComponents<AudioSource>();
        mech = gameObject.GetComponent<MechControll>();
    }

    // Update is called once per frame
    void Update()
    {
        if(partCount==6 && !allPartCollectedDone)
        {
            Debug.Log("ALL PARTS COLLECTED");
            StartCoroutine(onCollectEverything());
            allPartCollectedDone = true;
        }

        
        if(!gameDone && allPartCollectedDone && (Vector3.Distance(transform.position, target.transform.position) < minDistToTarget))
        {
            Debug.Log("GAME DONE!");
            mech.canControl = false;
            StartCoroutine(mechGameDone());
            gameDone = true;
        }

        if (gameDone && mech.canControl)
        {
            mech.canControl = false;
        }
    }

    IEnumerator onCollectEverything()
    {
        mech.canControl = false;
        GameObject canvas = null;
        foreach (Transform child in GameObject.Find("Bip001").GetComponentInChildren<Transform>())
        {
            if (child.gameObject.name == "compMess")
            {
                canvas = child.gameObject;
                break;
            }
        }
        canvas.SetActive(true);
        soundSource[3].Play();
        yield return new WaitForSeconds(9f);
        mech.canControl = true;
        canvas.SetActive(false);
    }

    IEnumerator mechGameDone()
    {
        foreach (GameObject o in holo_parts)
        {
            moveTowardsTarget script = o.AddComponent<moveTowardsTarget>();
            script.target = target;
            yield return new WaitForSeconds(1f);
        }
        yield return new WaitForSeconds(5f);
        com1.active = true;
        yield return new WaitForSeconds(1f);
        com2.active = true;
        yield return new WaitForSeconds(1f);
        com3.active = true;
        yield return new WaitForSeconds(8f);
        target.AddComponent<MoveTargetUp>();
        yield return new WaitForSeconds(3f);
        StartCoroutine(MissionAccomplished());
        yield return new WaitForSeconds(7f);
        GameObject.Find("LevelChanger").GetComponent<LevelChanger>().FadeToLevel("Scene_Credits");
    }

    IEnumerator MissionAccomplished()
    {
        GameObject canvas = null;
        foreach (Transform child in GameObject.Find("Bip001").GetComponentInChildren<Transform>())
        {
            Debug.Log("entered for loop");
            if (child.gameObject.name == "compMess")
            {
                canvas = child.gameObject;
                break;
            }
        }
        canvas.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Mission Accomplished";
        canvas.SetActive(true);
        yield return new WaitForSeconds(1.5f);
        canvas.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag!="parts")
        {
            return;
        }
        else
        {
            //display message saying other.name has been collected
            Debug.Log(other.name);
            foreach (GameObject o in holo_parts)
            {
                Debug.Log(o.name == other.name + "_holo");
                if(o.name==other.name+"_holo")
                {
                    GameObject holo_obj = o.gameObject;
#pragma warning disable CS0618 // Type or member is obsolete
                    bool active = holo_obj.active;
                    if (!active)
                    {
                        partCount++;
                        holo_obj.SetActive(true);
                    }
                    break;
                }
            }
            Destroy(other.gameObject);
        }
    }
}
#pragma warning restore CS0618 // Type or member is obsolete
