﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Positron;
    public class ChairInputWrapper : MonoBehaviour
{
    private ShipInput input;

    // 

    private float target;
    private float throttle;
    private float verticalSpeed;
    private float strafe;

    private float pitch;
    private float yaw;
    private bool hyperSpeed = false;
    private GameObject ship;

    // Start is called before the first frame update


    void Start()
    {

    }

    // Update is called once per frame
    private float pitchAngle = 0f;

    private float yaw_chair_angle = 0f;
    private float yaw_prev_ship_angle = 0f;
    private float yaw_curr_ship_angle = 0f;
    private float delta_yaw = 0f;
    private int rotationCount = 0;

    private float velocity = 50f;
    private float acceleration = 10f;

    float time = 0.0f;

    int rotations = 0;

    void Update()
    {
        if (ship == null)
        {
            ship = GameObject.Find("spaceShip");
            input = ship.GetComponent<ShipInput>();
            yaw_prev_ship_angle = yaw_curr_ship_angle =  ship.transform.eulerAngles.y;
            return;
        }

        if(input.CanControl())
        {
            time += Time.deltaTime;
            
            yaw_curr_ship_angle = ship.transform.eulerAngles.y;
            delta_yaw = yaw_curr_ship_angle - yaw_prev_ship_angle;
            if(yaw_prev_ship_angle < 10 && yaw_curr_ship_angle>350) //left turn
            {
                rotationCount--;
            }
            else if (yaw_prev_ship_angle > 350 && yaw_curr_ship_angle < 10 ) //right turn
            {
                rotationCount++;
            }
            Debug.Log(yaw_curr_ship_angle+":"+yaw_prev_ship_angle+":"+delta_yaw+":"+ rotationCount);
            yaw_prev_ship_angle = yaw_curr_ship_angle;
            yaw_chair_angle += delta_yaw; 
            
        }
    }
}
