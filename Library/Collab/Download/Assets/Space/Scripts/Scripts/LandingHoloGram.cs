﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LandingHoloGram : MonoBehaviour
{
    List<GameObject> terrain = new List<GameObject>();
    GameObject landingPad;
    GameObject map;
    GameObject spaceShip;
    Dictionary<string,GameObject> myMap = new Dictionary<string, GameObject>();
    public bool displayMap = true;
    private float temp;

    GameObject miniShip;
    GameObject miniLanding;
    public float maxDistance = 1000f;

    // Start is called before the first frame update
    void Start()
    {
        terrain.AddRange(GameObject.FindGameObjectsWithTag("ground"));
        map = GameObject.Find("landingHoloGram");
        spaceShip = GameObject.Find("spaceShip_position");
        miniShip = (GameObject) Instantiate(Resources.Load("WaspInterjector_prefab"), new Vector3(0, 0, 0), Quaternion.identity);
        landingPad = GameObject.Find("landingPad");
        miniLanding = (GameObject)Instantiate(Resources.Load("landingPad"), new Vector3(0, 0, 0), Quaternion.identity);
        miniLanding.transform.rotation = landingPad.transform.rotation;
        InstatiateEverything();

    }

    // Update is called once per frame
    void Update()
    {
        temp = Input.GetAxis("MapButton");
        if (temp == 1f)
            displayMap = true;
        else if(temp == -1f)
            displayMap = false;
        if (displayMap)
        {
            map.active = true;
            UpdateLandingPadPosition();
        }
        else
        {
            map.active = false;
        }
    }

    void InstatiateEverything()
    {
        miniShip.transform.localScale = new Vector3(0.002f, 0.002f, 0.002f);
        miniShip.transform.parent = map.transform;
        miniShip.transform.position = map.transform.position;
        miniShip.transform.rotation = spaceShip.transform.rotation;

        miniLanding.transform.localScale = new Vector3(0.002f, 0.002f, 0.002f);
        miniLanding.transform.parent = map.transform;
        Vector3 newVector = ((spaceShip.transform.position - landingPad.transform.position).normalized);
        newVector *= 0.1f;
        miniLanding.transform.position = miniShip.transform.position - newVector;

        //InstantiateTerrain();
    }

    void InstantiateTerrain()
    {
        foreach (GameObject t in terrain)
        {
            Debug.Log(t.name);
            //float dist = Vector3.Distance(planet.transform.position, sun.transform.position)/maxDistance;
            GameObject miniTerrain = Instantiate(t, new Vector3(0, 0, 0), Quaternion.identity);
            miniTerrain.transform.localScale = new Vector3(0.00005f, 0.00005f, 0.00005f);
            miniTerrain.transform.parent = map.transform;
            miniTerrain.transform.tag = "Untagged";
            //miniTerrain.GetComponent<TerrainCollider>().enabled = false;
            Vector3 newVector = ((spaceShip.transform.position - t.transform.position).normalized);
            newVector *= 0.1f;
            miniTerrain.transform.position = miniShip.transform.position - newVector;
        }
    }

    void UpdateLandingPadPosition()
    {
        float dist = ((Vector3.Distance(spaceShip.transform.position, landingPad.transform.position)) / maxDistance)/2;
        //Debug.Log(Vector3.Distance(spaceShip.transform.position, landingPad.transform.position)+","+dist);
        Vector3 newVector = ((spaceShip.transform.position - landingPad.transform.position).normalized);
        newVector *= dist;
        miniLanding.transform.position = miniShip.transform.position - newVector;
    }
}
